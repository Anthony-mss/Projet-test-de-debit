'use strict';

var room              = room,
    roomsList         = [],
    peers             = {},
    users             = {},
    protocol          = window.location.protocol,
    hostname          = window.location.hostname,
    rootUrl           = protocol + "//" + hostname,
    localStream       = undefined,
    remoteStream      = undefined,
    localScreen       = undefined,
    id                = null;
    

var onBistriConferenceReady = function()
{
    var daddy = window.parent;
    if(!bc.isCompatible()){
        alert("Non-compatible browser :()");
        return;
    }
 
    bc.init( {
        appId              : window.parent.appId,
        appKey             : window.parent.appKey,
        debug              : false
    } );

    bc.signaling.bind( "onConnected", function( data )
    {
        
        daddy.document.getElementById("connecting").style.fontWeight="normal";
        daddy.document.getElementById("waitingcamera").style.fontWeight="bold";
        id = data.id;
        bc.startStream( "webcam-hd", function( stream, pid )
        {
            daddy.document.getElementById("waitingcamera").style.fontWeight="normal";
            
            daddy.postMessage({streamAdded: true}, "*");
            localStream = stream;
            var room = testId;
            console.log("Room:", room)
            bc.joinRoom( room );
            bc.monitorAudioActivity( localStream, function( audioLevel ){                     
                document.getElementById('vuminside').style.height = (100 - audioLevel) + "px";
            } );
            
        }.bind(this) );
    } );

    bc.signaling.bind( "onJoinedRoom", function( data )
    {
        if(data.members.length) {
            bc.call(data.members[0].id, data.room, {sendonly:true, stream: localStream});
            bc.openDataChannel( data.members[ 0 ].id, "chat", data.room );
        }
        else alert("Error: server is not present in room.")
        bc.attachStream (localStream, document.getElementById('video'))
    } );
    
    bc.streams.bind ( "onStreamAdded", function( stream, pid )
    {
        //console.log("We received a stream in room1 but we do nothing with it.")
    /*
        console.log("Stream added");
        remoteStream = stream;
        bc.attachStream ( stream, document.getElementById('video') );
    */} );

    bc.signaling.bind( "onPeerJoinedRoom", function( data )
    {
        if ( !users[ data.pid ] )
        {
            users[ data.pid ] = {
                id: data.pid,
                name: data.name || 'unknown'
            };
        }
    } );

    bc.signaling.bind( "onPeerQuittedRoom", function( data )
    {
        bc.disconnect();
    } );
    
    bc.signaling.bind( "onDisconnected", function( data )
    {
        bc.stopStream (localStream, function( stream ) {
            bc.detachStream( stream );
        } );
    } );

    bc.streams.bind( "onStreamError", function( error ){
        alert("Error:", error);
    } );
    
    bc.channels.bind( "onDataChannelRequested", function ( dataChannel, remoteUserId ){
    });
    
    bc.channels.bind( "onDataChannelCreated", function ( dataChannel, remoteUserId ){
        dataChannel.onMessage = function( event ){
            console.log( event.data );
        };

        /*setInterval( function(){
            if(dataChannel.readyState == 'open') dataChannel.send( "Toto" );
        }, 1000 );
*/
    });
    

    //bc.connect();
};

