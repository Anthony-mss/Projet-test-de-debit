var testId = null;

window.addEventListener('message', function(event){
    if( !event.data.action ) return;
    switch( event.data.action ) {
        case "connect":
            bc.connect();
        break;
        case "disconnect":
            bc.disconnect();
        break;
        case "define-testid":
            testId = event.data.value;
        break;
    }
});