

function Ajax( props ){
	this.def = new Deferred();
	this.timer = null;
	this.abort = false;
	this.timeout = props.timeout || undefined;
	this.url = props.url || location.pathname;
	this.data = props.data || {};
	this.type = props.type || "GET";
	this.onSuccess = props.onSuccess || function(){};
	this.onError = props.onError || function(){};
	this.dataType = props.dataType || "text";
	this.xhr = new XMLHttpRequest();
	this.xhr.onreadystatechange = this._readystatechange.bind( this );
	return this._doRequest();
}

Ajax.prototype = {

	_get: function(){

		var parameters = "";

		if( Object.keys( this.data ).length ){
			parameters += "?";
			for( p in this.data ){
				if( parameters.length > 1 ){
					parameters += "&";
				}
				parameters += p + "=" + this.data[ p ];
			}
		}

		this.xhr.open( this.type, this.url + parameters, true );
		this.xhr.send( null );
	},

	_post: function(){
		this.xhr.open( this.type, this.url, true );
		this.xhr.setRequestHeader( "Content-type", "application/json" );
		this.xhr.send( JSON.stringify( this.data ) );
	},

	_doRequest: function(){
		this[ this.type == "POST" ? "_post" : "_get" ]();
		if( this.timeout ){
			this.timer = setTimeout( ( function(){
				this.abort = true;
				this.xhr.abort();
			} ).bind( this ), this.timeout * 1000 );
		}
		return this.def;
	},

	_readystatechange: function(){
		var status;
		if ( this.xhr.readyState == 4 ){
			if( this.timer ){
				clearTimeout( this.timer );
			}
			status = this.xhr.status;
			if( status == 200 ){
				if( typeof this.onSuccess === "function" ){
					var data;
					switch( this.dataType ){
						case "json":
							try{
								data = JSON.parse( this.xhr.responseText );
							}
							catch( e ){
								log( "[Ajax]", e );
							}
							break;
						case "xml":
							data = this.xhr.responseXML;
							break;
						case "text":
						default:
							data = this.xhr.responseText;
							break
					}
					this.def.resolve( data );
					this.onSuccess( data );
				}
			}
			else{
				if( typeof this.onError === "function" ){
					this.def.reject( this.xhr, this.abort ? "timeout" : "error", this.xhr.statusText );
					this.onError( this.xhr, this.abort ? "timeout" : "error", this.xhr.statusText );
				}
			}
		}
	}
}

function Deferred(){
	this._done = undefined;
	this._fail = undefined;
};

Deferred.prototype = {

	done: function( func ){
		if( typeof func !== "function" ){
			throw new Error( "function expected (Deferred.done)" );
		}
		else {
			this._done = func;
		}
		return this;
	},

	fail: function( func ){
		if( typeof func !== "function" ){
			throw new Error( "function expected (Deferred.fail)" );
		}
		else {
			this._fail = func;
		}
		return this;
	},

	resolve: function(){
		if( this._done ){
			this._done.apply( this._done, arguments );
		}
	},

	reject: function(){
		if( this._fail ){
			this._fail.apply( this._fail, arguments );
		}
	}
};

var xhr = function(){};

xhr.getText = function( url ){
    return new Ajax( {
        url: url,
        dataType: 'text'
    } );
}

xhr.getJSON = function( url ){
    return new Ajax( {
        url: url,
        dataType: 'json'
    } );
}

xhr.getXML = function( url ){
    return new Ajax( {
        url: url,
        dataType: 'xml'
    } );
}

xhr.POST = function( url, data, dataType ){
    return new Ajax( {
    	type: 'POST',
        url: url,
        data: data,
        dataType: dataType || 'text'
    } );
}


xhr.GET = function( url, data, dataType ){
    return new Ajax( {
    	type: 'GET',
        url: url,
        data: data,
        dataType: dataType || 'text'
    } );
}

